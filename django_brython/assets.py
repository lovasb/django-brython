import sys
from browser import window, ajax


def require(filename):
    BRYTHON = getattr(window, '$B')

    path = BRYTHON.path[0].rstrip('/')
    python_path = sys._getframe().f_back.f_code.co_filename
    python_path = python_path.replace(path, '', 1)

    def file_downloaded(resp):
        if resp.status == 200:
            setattr(getattr(window, '$B'), 'django_brython', resp.text)

    ajax.get(
        url=f'{path}/@/require/?caller={python_path}&required={filename}',
        blocking=True,
        oncomplete=file_downloaded
    )

    return getattr(BRYTHON, 'django_brython', None)
