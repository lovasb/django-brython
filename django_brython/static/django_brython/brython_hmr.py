import importlib, sys
from browser import document


def on_reload_brython_module(evt):
    try:
        module = sys.modules[evt.detail['module_name']]
        importlib.reload(module)
    except KeyError:  # this module is not imported by client
        return


document.bind('reload_brython_module', on_reload_brython_module)
