1.5.5
-----

Improvements:

    - *require* function for asset management in source codes (this function needs Python>=3.9)
