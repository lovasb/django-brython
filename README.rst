
**django-brython** is a reusable application to ease the development for a Django based backend application with Python/Brython frontend.

This solution use the Django's standard tools (reusable applications and static files), so the client side Brython code can developed, used, and distributed same way as the serve side ones.

**Documentation**\ : `link <https://django-brython.lovasb.com/>`_
