.. django-brython documentation master file, created by
   sphinx-quickstart on Fri Nov 27 14:06:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-brython's documentation!
==========================================

.. include:: README.rst

.. toctree::
   :caption: Installation and configuration
   :maxdepth: 1

   install
   quickstart

.. toctree::
   :caption: Testing your code
   :maxdepth: 1

   testing

.. toctree::
   :caption: Move to production
   :maxdepth: 1

   deployment

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
