.. _deployment:

Deploy Brython code with Django
-------------------------------

During the development *django-brython* can serve any Python code from the $PATH. In the production environment it is not accepted to download any Python files from server. If the DEBUG settings is False, then the Python file serving doesn't work anymore. Working with Brython in production, you need to bundle the files. *django-brython* integrates into the Django staticfiles ecosystem. To get things working, you need to setup Brythons static file finder and the main/entry module of your client codes. In the previous examples the *frontend* application is used as the main module.

.. code-block:: python

    # project/settings.py

    STATICFILES_FINDERS = [
        ...
        'django_brython.staticfiles.BrythonStaticGenerator',
    ]

    BRYTHON_BUNDLER_MAIN_MODULE = 'frontend'

After this try to use the collectstatic::

    python manage.py collectstatic

*django-brython* generates Brython/Javascript file into your apps static directory::

    static/
      frontend/
         js/
           frontent.brython.js

Insert the brython.js into the HTML template, and you're done:

.. code-block::

    <head>
        ...
        <script type="text/javascript" src="{% static 'django_brython/js/brython.js' %}"></script>
        <script type="text/javascript" src="{% static 'django_brython/js/brython_stdlib.js' %}"></script>

        {% if debug %}
        <script type="text/python" src="brython/frontend/main.py"></script>
        {% else %}
        <script type="text/javascript" src="{% static 'frontend/js/frontend.brython.js' %}"></script>
        <script type="text/python">
            from frontend import main
            main.display()  # call entry point from main
        </script>
        {% endif %}
    </head>

    <body onload="brython({% if debug %}{debug: 1, pythonpath: ['/brython/']}{% endif %})">
    </body>


The frontend application Python files are bundled into frontend.brython.js, so there is no need to load them separately from server.


Exclude from bundler
^^^^^^^^^^^^^^^^^^^^

In the frontend app there are some files, which the bundler shouldn't pack into output. For example the unittest files. You can exclude any module, with listing in the BRYTHON_BUNDLER_EXCLUDE settings:

.. code-block:: python

    # project/settings.py

    BRYTHON_BUNDLER_EXCLUDE = [
        'frontend.apps'  # exclude frontend/apps.py
        'frontend.tests',  # exclude the frontend/tests/*.py
    ]


Bundling asset files
^^^^^^^^^^^^^^^^^^^^

Sometimes it's necessary to include static file assets into bundled Brython code. For example if you use templates which are rendered on client side,
it's easier to maintain these assets in separated files. **django-brython** has special *require* function for that.

.. code-block:: python

    from browser import document, html
    from django_brython.assets import require

    document <= html.H1(require('templates/test.html'), Id="main")  # path relative to python module

During the development the require function downloads the asset file from the server each time when it's called. The path passed to require function must be relative to Python module from where it's called. Eg. calling the *require* in frontend/main.py, the asset file correct path is frontend/templates/test.html.

If the test.html content is: 'Exmaple content', then the output is:

.. code-block::

    <html>
        ...
        <body>
            ...
            <h1>Example content</h1>
        </body>
    </html>


If you bundle the client application, then the *require* function is replaced by the content of asset file. So the output will be:

.. code-block:: python

    from browser import document, html
    # require import is removed

    document <= html.H1('Example content', Id="main")  # Load static content into bundled output
