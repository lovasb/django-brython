.. _quickstart:

Quick start guide
-----------------

First you'll need to have Django and django-brython
installed; for details on that, see :ref:`the installation guide <install>`.

After install add `"django_brython"` to your
:data:`~django.conf.settings.INSTALLED_APPS` setting, and include the django-brython urls, into project's main url config:

.. code-block:: python

   # project/settings.py

   INSTALLED_APPS = [
        ...
        'django_brython',
        ...
    ]

.. code-block:: python

    # project/urls.py

    urlpatterns = [
        ...
        path('brython/', include('django_brython.urls', namespace='brython'))
        ...
    ]

To illustrate how to build Brython frontend to your Django backend project, let's create two application: backend, frontend. In the *backend* application will be the "classic" server side codebase, in the *frontend* app lives the Brython (client side) code.

So create the apps, and add to INSTALLED_APPS::

    django-admin startapp frontend
    django-admin startapp backend

.. code-block:: python

   # project/settings.py

   INSTALLED_APPS = [
        ...
        'frontend',
        'backend'
        ...
    ]


Create a simple view in backend app:

.. code-block:: python

    # backend/views.py

    from django.shortcuts import render


    def index(request):
        return render(request, 'index.html')


Create index.html template for the view:

.. code-block:: html

    {% load static %}
    <html lang="en">
      <head>
        <meta charset="utf-8">

        <!-- Include the brython javascript resources -->
        <!-- This is standard Brython distribution, of course this files can be loaded from any CDN -->
        <script type="text/javascript" src="{% static 'django_brython/js/brython.js' %}"></script>
        <script type="text/javascript" src="{% static 'django_brython/js/brython_stdlib.js' %}"></script>
      </head>
      <!-- Initialize Brython, with the path prefix what you specified earlier in urls.py -->
      <body onload="brython({debug: 1, pythonpath: ['/brython/']})">
        Index
      </body>
    </html>

Create urls.py for your backend app, and include it to project's main urls:

.. code-block:: python

    # backend/urls.py

    from django.urls import path

    from . import views

    urlpatterns = [
        path('index/', views.index, name='index')
    ]

    app_name = 'backend'



.. code-block:: python

    # project/urls.py

    urlpatterns = [
        ...
        path('backend/', include('backend.urls', namespace='backend')),
        ...
    ]


Create main.py in our frontend application:

.. code-block:: python

    # frontend/main.py

    from browser import document, html

    print('Hello World from Brython')
    # Insert Header into document body
    document <= html.H1("HELLO FROM BRYTHON", Id="main")


Call the main module in the client side code, modify the index.html:

.. code-block:: html

    ...
    <head>
        <script type="text/python" src="/brython/frontend/main.py"></script>
    </head>
    ...


Start your dev server and open the index page::

    http://localhost:8000/backend/index/


You'll see the the H1 content: 'HELLO FROM BRYTHON'. Congratulation!

How it is working?

    1. The index page is loaded, the onload event calls Brython ecosystem
    2. Brython VFS (Virtual File System) loads the linked main.py
    3. django_brython module searches the frondend.main on the $PATH, and if found the content is served as static file
    4. Brython evaluate the static file content, and inserts H1 into the body
