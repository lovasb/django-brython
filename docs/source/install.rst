.. _install:

Installation
------------

The preferred method of installing django-brython is via `pip`,
the standard Python package-installation tool. If you don't have
`pip`, instructions are available for `how to obtain and install it
<https://pip.pypa.io/en/latest/installing.html>`_, though if you're
using a supported version of Python, `pip` should have come bundled
with your installation of Python.

Once you have `pip`, type::

    pip install django-brython

..
    If you don't have a copy of a compatible version of Django, this will
    also automatically install one for you.

Install development version
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some features are available only in the development version. To install the development version, type::

    pip install git+https://gitlab.com/lovasb/django-brython.git

..