from django.shortcuts import render


def index(request):
    # valami 212345678
    return render(request, 'index.html')


def tests(request):
    return render(request, 'tests.html')
