from django.test import LiveServerTestCase, override_settings
from selenium import webdriver

from .brython import BrythonTest


class Test1(LiveServerTestCase, BrythonTest):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()
