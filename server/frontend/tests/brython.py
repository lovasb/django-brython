from django_brython.testing import location

from browser import document

from django_brython.testing import reverse


class BrythonTest:
    @location(reverse('backend:index'), wait_for=lambda: document.getElementById("main") is not None)
    def test_example(self):
        h = document.getElementById("main")
        assert h.text == "HELLO FROM BRYTHON"
